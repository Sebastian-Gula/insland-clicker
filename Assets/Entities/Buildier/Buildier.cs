﻿using UnityEngine;

public class Buildier : MonoBehaviour
{
    enum BuildingState { NONE, ACTIVE, PLACED }

    public GameObject[] Buildings;
    public GameObject Options;

    public bool _overTerrain;
    public bool _overBuilding;
    GameObject _building;
    BuildingState _currentState = BuildingState.NONE;


    public void SpawnBuilding(int index)
    {
        if (_currentState != BuildingState.PLACED)
        {
            if (_currentState == BuildingState.ACTIVE)
                Destroy(_building);

            _currentState = BuildingState.ACTIVE;
            _building = GameObject.Instantiate(Buildings[index]);
        }
    }

    public void Accept()
    {
        if (_currentState == BuildingState.PLACED)
        {
            _currentState = BuildingState.NONE;

            Options.SetActive(false);
        }
    }

    public void Reject()
    {
        if (_currentState == BuildingState.PLACED)
        {
            _currentState = BuildingState.ACTIVE;

            Options.SetActive(false);
        }
    }


    void Update()
    {
        if (_currentState == BuildingState.ACTIVE)
        {
            UpdateBuildingPosition();
            RotateBuilding();
            PlaceBuilding();
            DeselectBuilding();
        }
    }

    void UpdateBuildingPosition()
    {
        RaycastHit hitInfo;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out hitInfo))
        {
            var localScale = _building.transform.localScale;
            var rotation = _building.transform.eulerAngles.y;
            var rotated = rotation.Equals(90) || rotation.Equals(270);
            var localScaleX = rotated ? localScale.z : localScale.x;
            var localScaleZ = rotated ? localScale.x : localScale.z;

            int x = Mathf.RoundToInt(Mathf.Clamp(hitInfo.point.x, localScaleX / 2f, 50 - localScaleX / 2f));
            int z = Mathf.RoundToInt(Mathf.Clamp(hitInfo.point.z, localScaleZ / 2f, 50 - localScaleZ / 2f));

            _building.transform.position = new Vector3(x, localScale.y / 2f, z);
        }

        _overTerrain = Physics.Raycast(ray, out hitInfo, Mathf.Infinity, LayerMask.GetMask("Terrain"));
        Physics.Raycast(ray, out hitInfo, Mathf.Infinity, LayerMask.GetMask("Buildings"));

        if (hitInfo.collider && hitInfo.collider.gameObject != _building)
            _overBuilding = true;
        else
            _overBuilding = false;
    }

    void RotateBuilding()
    {
        if (Input.GetAxis("Mouse ScrollWheel") != 0)
            _building.transform.localRotation *= Quaternion.Euler(0, Input.GetAxis("Mouse ScrollWheel") > 0 ? 90 : -90, 0);
    }

    void DeselectBuilding()
    {
        if (Input.GetMouseButtonDown(1))
        {
            _currentState = BuildingState.NONE;
            Destroy(_building);
        }
    }

    void PlaceBuilding()
    {
        if (_overTerrain && !_overBuilding && Input.GetMouseButtonDown(0))
        {
            _currentState = BuildingState.PLACED;

            Options.SetActive(true);
            Options.transform.position = _building.transform.position - Camera.main.transform.forward * 5;
            Options.transform.localRotation = Quaternion.Euler(39, 45, 0);
        }
    }
}
