﻿using UnityEngine;

public class MainCamera : MonoBehaviour
{
    const float MinOrthographicSize = 5;
    const float MaxOrthographicSize = 22;

    [Range(0.1f, 2)]
    public float MoveSpeed;
    [Range(0.01f, 0.1f)]
    public float ZoomSpeed;

    readonly float _topBoundary = 23;
    readonly float _bottomBoundary = -24;
    readonly float _leftBoundary = -41;
    readonly float _rightBoundary = 38.5f;


    //void Start()
    //{
    //    Vector3 forward;
    //    Vector3 right;

    //    forward = Camera.main.transform.forward;
    //    forward.y = 0;
    //    forward.Normalize();

    //    right = Camera.main.transform.right;
    //    right.y = 0;
    //    right.Normalize();
    //}

    void Update()
    {
        if (Input.touchCount == 1)
            Move();
        else if (Input.touchCount == 2)
            PinchToZoom();
    }

    void Move()
    {
        Touch currentTouch = Input.GetTouch(0);

        if (currentTouch.phase == TouchPhase.Moved)
        {
            Vector3 translation = -currentTouch.deltaPosition * Time.deltaTime * MoveSpeed;
            MoveInsideTheBordersIfOutside(translation);
        }
    }



    void PinchToZoom()
    {
        Touch touchZero = Input.GetTouch(0);
        Touch touchOne = Input.GetTouch(1);

        Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
        Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

        float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
        float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;
        float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

        Camera.main.orthographicSize += deltaMagnitudeDiff * ZoomSpeed;
        Camera.main.orthographicSize = Mathf.Clamp(Camera.main.orthographicSize, MinOrthographicSize, MaxOrthographicSize);

        MoveInsideTheBordersIfOutside(Vector3.zero);
    }


    void MoveInsideTheBordersIfOutside(Vector3 translation)
    {
        float topDiff = UnderTheTopEdge(translation.y);
        float bottomDiff = AboveTheBottomEdge(translation.y);
        float leftDiff = RightFromTheLeftEdge(translation.x);
        float rightDiff = LeftFromTheRightEdge(translation.x);

        if (topDiff < 0)
            translation += new Vector3(0, topDiff, 0);

        if (bottomDiff > 0)
            translation += new Vector3(0, bottomDiff, 0);

        if (leftDiff > 0)
            translation += new Vector3(leftDiff, 0, 0);

        if (rightDiff < 0)
            translation += new Vector3(rightDiff, 0, 0);

        Camera.main.transform.Translate(translation);
    }

    float UnderTheTopEdge(float translationY)
    {
        float topCameraEdge = Camera.main.transform.localPosition.y + Camera.main.orthographicSize + translationY;
        return _topBoundary - topCameraEdge;
    }

    float AboveTheBottomEdge(float translationY)
    {
        float bottomCameraEdge = Camera.main.transform.localPosition.y - Camera.main.orthographicSize + translationY;
        return _bottomBoundary - bottomCameraEdge;
    }

    float RightFromTheLeftEdge(float translationX)
    {
        float leftCameraEdge = Camera.main.transform.localPosition.x - Camera.main.orthographicSize * Camera.main.aspect + translationX;
        return _leftBoundary - leftCameraEdge;
    }

    float LeftFromTheRightEdge(float translationX)
    {
        float rightCameraEdge = Camera.main.transform.localPosition.x + Camera.main.orthographicSize * Camera.main.aspect + translationX;
        return _rightBoundary - rightCameraEdge;
    }


    Vector3 GetWorldPoint(Vector2 screenPoint)
    {
        RaycastHit hit;
        Physics.Raycast(Camera.main.ScreenPointToRay(screenPoint), out hit);
        return hit.point;
    }
}